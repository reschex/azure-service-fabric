#! /bin/bash
sudo rm -rf Exchange/*
# run unit tests
sudo dotnet test sfchallenge-master/UserStore.Tests
if [[ $? -ne 0 ]]
then
    exit 1
fi
sudo dotnet test sfchallenge-master/OrderBook.Tests
if [[ $? -ne 0 ]]
then
    exit 1
fi
sudo pwsh sfchallenge-master/Scripts/xplat-build.ps1
mv sfchallenge-master/publish/* Exchange/