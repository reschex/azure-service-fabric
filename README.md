# Azure Service Fabric Hackathon

## Team Implied Intelligence

### Environment Setup

    vagrant plugin install vagrant-disksize vagrant-vbguest
    vagrant up

The bootstrap.sh, which provisions the vagrant box, follows some of the instructions from
<https://docs.microsoft.com/en-us/azure/service-fabric/service-fabric-get-started-linux#set-up-a-local-cluster>

We're not building the local cluster in the vagrant environment as the challenge application has been build for a windows runtime and is not deployable onto a Linux cluster.

### Build and deploy

    ./build.sh
    ./deploy.sh

have been provided to build and deploy the application. Currently, you have to delete the application from the cluster before re-deploying.

## //TO DO

    - fix unit tests
    - build in integration tests into build
    - make version the commit hash
    - upgrade deployment rather than delete/re-deploy