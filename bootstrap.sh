#!/bin/sh

sudo curl -s https://raw.githubusercontent.com/Azure/service-fabric-scripts-and-templates/master/scripts/SetupServiceFabric/SetupServiceFabric.sh | sudo bash

# Install dotnet sdk
wget -q https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb

sudo apt-get update

sudo apt-get install apt-transport-https -y
sudo apt-get install dotnet-sdk-2.1 -y
sudo apt-get install powershell -y 

# python3 for service fabric cli
#sudo apt-get install python3 -y
#sudo apt-get install python3-pip -y
#sudo pip3 install sfctl

export PATH=$PATH:~/.local/bin
echo "export PATH=$PATH:~/.local/bin" >> ~/.bashrc

# service fabric aliases for choosing cluster
openssl pkcs12 -in /vagrant/kv*.pfx -out sf-azure.pem -nodes -passin pass:
openssl pkcs12 -in /vagrant/gordsfchallenge.pfx -out sf-azure-b.pem -nodes -passin pass:OdkaF1uRPUDRWPvvW2Ub/POjTdhbjcGZma7bRDLZR7U=

echo 'alias sfa="sfctl cluster select --endpoint https://team7-ldn0618cluster.westeurope.cloudapp.azure.com:19080 --pem sf-azure.pem --no-verify"' >> .bash_aliases
echo 'alias sfb="sfctl cluster select --endpoint https://sfchallenge2018b.eastus.cloudapp.azure.com:19080 --pem sf-azure-b.pem --no-verify"' >>.bash_aliases
echo 'alias sfl="sfctl cluster select --endpoint http://localhost:19080"' >> .bash_aliases
echo 'alias sf="sfctl"' >> .bash_aliases