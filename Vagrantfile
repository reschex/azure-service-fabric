#!/usr/bin/env ruby
# Controls and provisions a virtual development environment.

# read settings file
require "yaml"
settings = YAML.load_file("settings.yaml")

# check for requried plugins
settings["plugins"].each do |plugin|
  unless Vagrant.has_plugin?(plugin)
    raise "Missing plugin! Run: vagrant plugin install " + plugin
  end
end

# configure the virtual machine
Vagrant.configure(2) do |config|

  config.vm.box = settings["box"]
  config.vm.hostname = settings["hostname"]
  config.disksize.size = settings["disksize"]

  # set the name and memory in virtual box
  config.vm.provider "virtualbox" do |vb|
    vb.name = settings["name"]
	vb.cpus = settings["cpus"]
    vb.memory = settings["memory"]
  end
  
  settings["ports"].each do |port|
     ports = port.split(":")
     config.vm.network :forwarded_port, host: ports[0], guest: ports[1]
  end unless settings["ports"].nil?

  config.vm.synced_folder ".", "/vagrant", type: "virtualbox"

  # set vagrant users home directory
  # this starts vagrant ssh sessions in the appropriate directroy
  config.vm.provision :shell, privileged: false, inline: <<-SHELL
    grep -q 'cd /vagrant' ~/.bashrc || echo 'cd /vagrant' >> ~/.bashrc
  SHELL

  config.vm.provision :shell, path: 'bootstrap.sh'
end
